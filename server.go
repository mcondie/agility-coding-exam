package main

import (
	"log"
	"net/http"
	"os"
	"star-wars/graph"
	"star-wars/services"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	characterService := services.CharacterService{}
	starshipService := services.StarshipService{}
	planetService := services.PlanetService{}
	speciesService := services.SpeciesService{}
	srv := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: &graph.Resolver{
		CharacterService: &characterService,
		StarshipService:  &starshipService,
		PlanetService:    &planetService,
		SpeciesService:   &speciesService,
	}}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
