package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"star-wars/graph/model"
)

type SpeciesService struct {
}

type speciesResponse struct {
	Name     string `json:"name"`
	Language string `json:"language"`
	Lifespan string `json:"average_lifespan"`
}

func (s *SpeciesService) GetSpeciessForCharacter(character *model.Character) ([]*model.Species, error) {
	speciess := make([]*model.Species, len(character.Species))
	for i, ship := range character.Species {
		shipModel, err := s.GetSpeciesByURL(ship.Url)
		if err != nil {
			fmt.Printf("species request failed: %s\n", err)
			return nil, errors.New("could not request species")
		}
		speciess[i] = shipModel
	}
	return speciess, nil
}

func (s *SpeciesService) GetSpeciesByURL(url string) (*model.Species, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Printf("could not create species request: %s\n", err)
		return nil, errors.New("could not create species request")
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("error requesting species data: %s\n", err)
		return nil, errors.New("could not execute species request")
	}

	jsonData, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("error reading species data: %s\n", err)
		return nil, errors.New("could not read species response")
	}

	var response speciesResponse

	err = json.Unmarshal([]byte(jsonData), &response)
	if err != nil {
		fmt.Printf("error parsing species data: %s\n", err)
		return nil, errors.New("could not parse species response")
	}

	return &model.Species{
		Name:     response.Name,
		Language: response.Language,
		Lifespan: response.Lifespan,
	}, nil
}
