package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"star-wars/graph/model"
)

type PlanetService struct {
}

type planetResponse struct {
	Name       string `json:"name"`
	Population string `json:"population"`
	Climate    string `json:"climate"`
}

func (s *PlanetService) GetPlanetByURL(url string) (*model.Planet, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Printf("could not create planet request: %s\n", err)
		return nil, errors.New("could not create planet request")
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("error requesting planet data: %s\n", err)
		return nil, errors.New("could not execute planet request")
	}

	jsonData, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("error reading planet data: %s\n", err)
		return nil, errors.New("could not read planet response")
	}

	var response planetResponse

	err = json.Unmarshal([]byte(jsonData), &response)
	if err != nil {
		fmt.Printf("error parsing planet data: %s\n", err)
		return nil, errors.New("could not parse planet response")
	}

	return &model.Planet{
		Name:       response.Name,
		Population: response.Population,
		Climate:    response.Climate,
	}, nil
}
