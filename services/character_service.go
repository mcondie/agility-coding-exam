package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"star-wars/graph/model"
)

type CharacterService struct {
}

type characterSearchResponse struct {
	Results []characterResponse `json:"results"`
	Count   int                 `json:"count"`
	Next    string              `json:"next"`
}

type characterResponse struct {
	Name      string   `json:"name"`
	Starships []string `json:"starships"`
	Planet    string   `json:"homeworld"`
	Species   []string `json:"species"`
}

func (s *CharacterService) SearchForCharacter(search string) ([]*model.Character, error) {
	url := fmt.Sprintf("https://swapi.dev/api/people/?search=%s", search)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Printf("could not create request: %s\n", err)
		return nil, errors.New("could not create character request")
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("error requesting character data: %s\n", err)
		return nil, errors.New("could not execute character request")
	}

	jsonData, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("error reading character data: %s\n", err)
		return nil, errors.New("could not read character response")
	}

	var response characterSearchResponse

	err = json.Unmarshal([]byte(jsonData), &response)
	if err != nil {
		fmt.Printf("error parsing character data: %s\n", err)
		return nil, errors.New("could not parse character response")
	}

	characters := make([]*model.Character, len(response.Results))

	for i, c := range response.Results {
		starships := make([]*model.Starship, len(c.Starships))
		for j, ship := range c.Starships {
			starships[j] = &model.Starship{
				Url: ship,
			}
		}

		species := make([]*model.Species, len(c.Species))
		for k, spec := range c.Species {
			species[k] = &model.Species{
				Url: spec,
			}
		}

		characters[i] = &model.Character{
			Name:      c.Name,
			Starships: starships,
			Species:   species,
			Planet: &model.Planet{
				Url: c.Planet,
			},
		}
	}
	return characters, nil
}
