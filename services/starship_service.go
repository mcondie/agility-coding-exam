package services

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"star-wars/graph/model"
)

type StarshipService struct {
}

type starshipResponse struct {
	Name     string `json:"name"`
	Capacity string `json:"cargo_capacity"`
	Class    string `json:"starship_class"`
}

func (s *StarshipService) GetStarshipsForCharacter(character *model.Character) ([]*model.Starship, error) {
	starships := make([]*model.Starship, len(character.Starships))
	for i, ship := range character.Starships {
		shipModel, err := s.GetStarshipByURL(ship.Url)
		if err != nil {
			fmt.Printf("starship request failed: %s\n", err)
			return nil, errors.New("could not request starship")
		}
		starships[i] = shipModel
	}
	return starships, nil
}

func (s *StarshipService) GetStarshipByURL(url string) (*model.Starship, error) {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		fmt.Printf("could not create starship request: %s\n", err)
		return nil, errors.New("could not create starship request")
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("error requesting starship data: %s\n", err)
		return nil, errors.New("could not execute starship request")
	}

	jsonData, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("error reading starship data: %s\n", err)
		return nil, errors.New("could not read starship response")
	}

	var response starshipResponse

	err = json.Unmarshal([]byte(jsonData), &response)
	if err != nil {
		fmt.Printf("error parsing starship data: %s\n", err)
		return nil, errors.New("could not parse starship response")
	}

	return &model.Starship{
		Name:     response.Name,
		Capacity: response.Capacity,
		Class:    response.Class,
	}, nil
}
