# Agility Coding Exam

This project creates a graphql api using (https://gqlgen.com/getting-started/).

To start, run `go run server.go` and then navigate your favorite browser to `http://localhost:8080`

#### Sample query
```
query CharacterSearch($input: CharacterSearchInput!) {
  characterSearch(input:$input) {
    name
    starships {
      name
      class
      capacity
    }
    planet {
      name
      population
      climate
    }
    species {
      name
      language
      lifespan
    }
  }
}
```
#### Sample variables
```
{
  "input": {
    "search": "yoda"
  }
}
```