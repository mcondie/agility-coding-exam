package graph

import "star-wars/services"

//go:generate go run github.com/99designs/gqlgen generate

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	CharacterService *services.CharacterService
	StarshipService  *services.StarshipService
	PlanetService    *services.PlanetService
	SpeciesService   *services.SpeciesService
}
